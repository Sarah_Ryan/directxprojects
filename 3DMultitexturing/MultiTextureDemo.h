#pragma once

#ifndef _CUBE_DEMO_H_
#define _CUBE_DEMO_H_

#include "DX11DemoBase.h"
#include<xnamath.h>
#include <d3dx11effect.h>

class MultiTextureDemo : public Dx11DemoBase {

public:
	MultiTextureDemo();
	virtual ~MultiTextureDemo();

	bool LoadContent();
	void UnLoadContent();

	void Update(float dt);
	void Render();


private:

	ID3DX11Effect* effect_; //Object to load effect into
	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;
	//Index buffer used to specify array indices into a vertex list to specify what vertices form triangles.
	//Saves memory  for larger models
	ID3D11Buffer* indexBuffer_;

	//Object shaders use to access resources
	//When load tex into memory, must use it to access data via shader, will be binding to IA
	//CAN also provide general purpose data for parallel computing and other things
	//Allows us to view buffer in shader
	ID3D11ShaderResourceView* colorMap_;
	ID3D11ShaderResourceView* secondMap_;


	//Can access sampling state info of a tex
	//Allows us to set properties like tex filtering and addressing
	ID3D11SamplerState* colorMapSampler_;


	XMMATRIX viewMatrix_;
	XMMATRIX projMatrix_;


};

#endif