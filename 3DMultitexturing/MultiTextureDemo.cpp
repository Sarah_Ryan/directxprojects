#include "MultiTextureDemo.h"
#include <xnamath.h>


struct VertexPos {

	//Struct with X, Y, Z floating point values within
	//XM refers to XNA math
	//Float refers to data type of members
	//3 refers to how many numbers the structure has
	XMFLOAT3 pos;

	//Performing texture mapping....
	//Need to update vertex structure to including 2 floating point variables for TU and TV tex coordinates
	XMFLOAT2 tex0;

};



MultiTextureDemo::MultiTextureDemo() : effect_(0), inputLayout_(0),
vertexBuffer_(0), indexBuffer_(0), colorMap_(0), colorMapSampler_(0)
{

}

MultiTextureDemo::~MultiTextureDemo() {


}

void MultiTextureDemo::UnLoadContent() {

	//releases memory used by objects
	if (colorMapSampler_) colorMapSampler_->Release();
	if (colorMap_) colorMap_->Release();
	if (secondMap_) secondMap_->Release();
	if (inputLayout_) inputLayout_->Release();
	if (vertexBuffer_) vertexBuffer_->Release();

	colorMapSampler_ = 0;
	colorMap_ = 0;
	inputLayout_ = 0;
	vertexBuffer_ = 0;

}

bool MultiTextureDemo::LoadContent() {

	ID3DBlob* buffer = 0;

	//load vertex shader from file - name because shaders work together to shade surface with solid green colour
	//Load vetex from text file, compile into byte code OR DX11 will do for us during startup

	//This function call is the same as D3DX11CompileFromFile, function is written in DemoBase cpp
	//Params: Path of the HLSL shader code to be loaded and compiled, global macros within shaders code (N/A),
	//Macros work in HLSL same as they work in C/C++ - Defined in app side use type shader macro 
	//optional param for handling include statements that exist in HLSL file - used to specify behaviour for opening/closing files included in shader source (N/A),
	//Function name for shader you are compiling. File can have shader types and functions - this param points to entry point of shader we are compiling,
	//specifies shader model (4.0 = DX10 and above), (5.0 = DX11 hardware and above), memory address shader will reside

	//There are more parameters for the CompileFromFile function but we aren't using them. Can look up if needed.
	bool compileResult = CompileD3DShader("MultiTexture.fx", 0, "fx_5_0", &buffer);

	//Error handling
	if (compileResult == false) {
		MessageBox(0, "Error loading vertex shader", "Compile Error", MB_OK);
		return false;
	}

	HRESULT d3dResult;

	//Create vertex shader after compiled from source
	//Params: buffer for compiled code, its size in bytes, pointer to class linkage type, pointer to vertex shader object we are creating
	d3dResult = D3DX11CreateEffectFromMemory(buffer->GetBufferPointer(), buffer->GetBufferSize(), 0, d3dDevice_, &effect_);

	if (FAILED(d3dResult)) {
		if (buffer)
			buffer->Release();

		return false;

	}

	//Create vertex layout
	//Vertex layout is validated against vertex shader signature, we need at least vertex shader loaded into memory
	//Describe vertex layout of a vertex structure
	D3D11_INPUT_ELEMENT_DESC solidColorLayout[] =
	{
		//First member - Semantic name that describes purpose of element
		//POSITION = Used for vertex's position. 
		//Other examples: COLOR, NORMAL etc
		//Semantic binds element to HLSL shader's input/output variable
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		//Element for tex coordinate, semantic TEXCORD
		//R32G32 as we are only using 2 float values
		//offset is 12 as there is a float 3 in beginning for position, 
		//tex coordinates won't appear untly after 12 byte
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}

	};

	unsigned int totalLayoutElements = ARRAYSIZE(solidColorLayout);

	ID3DX11EffectTechnique* colorInvTechnique;
	colorInvTechnique = effect_->GetTechniqueByName("MultiTexture");
	ID3DX11EffectPass* effectPass = colorInvTechnique->GetPassByIndex(0);

	D3DX11_PASS_SHADER_DESC passDesc;
	D3DX11_EFFECT_SHADER_DESC shaderDesc;
	effectPass->GetVertexShaderDesc(&passDesc);
	passDesc.pShaderVariable->GetShaderDesc(passDesc.ShaderIndex, &shaderDesc);


	//Input layout created
	//Params: array of elements in vertex layout, number of elements in array, 
	//compiled vertex shader code with input signature that will be validated against array of elements,
	//size of shader's byte code, AND pointer of the object that will be created with this function call
	//Input signature MUST match input layout or function call will fail

	//Must use vertex shader 
	d3dResult = d3dDevice_->CreateInputLayout(solidColorLayout, totalLayoutElements, shaderDesc.pBytecode, shaderDesc.BytecodeLength,  &inputLayout_);

	buffer->Release();

	if (FAILED(d3dResult)) {
		return false;
	}


	//Triangle, half unit in size along X and Y
	//Also 0.5 along Z so that it will be visible on screen.
	//Won't render if cam is too close or behind surface
	VertexPos vertices[] = {

		//X,Y,Z for each vertex and texture coordinates
		{ XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) }, //top right
		{ XMFLOAT3(1.0f,  1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) }, //botton right
		{ XMFLOAT3(1.0f,  1.0f,  1.0f), XMFLOAT2(1.0f, 1.0f) }, //bottom left
		{ XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f,  1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(1.0f, -1.0f,  1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f,  1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f,  1.0f,  1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f,  1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f,  1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f,  1.0f,  1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT2(0.0f, 1.0f) },
	};



	//Above vertex list stored in array (vertices) and provided as a sub resource data 
	//Used during CreateBuffer call to create actual vertex buffer

	//Vertex buffer setup

	//Create buffer description
	//Used to provide details of the buffer we are creating, important as technically could be something other than vertex buffer
	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));

	vertexDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeof(VertexPos) * 24;

	//Subresource creation
	//Used (in this case) to pass vertex data to buffers creation function so buffer is filled
	//Can just pass NULL into creation = empty buffer. But we know what we want to store in it
	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));

	// is pointer to initialized memory OR in our case, memory we are sending to fill buffer with
	resourceData.pSysMem = vertices;

	//create actual vertex buffer
	//Params: buffer description, optional sub resource data and pointer for buffer object, defined by descriptor
	d3dResult = d3dDevice_->CreateBuffer(&vertexDesc, &resourceData, &vertexBuffer_);

	if (FAILED(d3dResult)) {
		DXTRACE_MSG("Failed to create vertex buffer");
		return false;
	}

	//Index buffer indices
	WORD indices[] = {
		3,   1,  0,  2,  1,  3,
		6,   4,  5,  7,  4,  6,
		11,  9,  8, 10,  9, 11,
		14, 12, 13, 15, 12, 14,
		19, 17, 16, 18, 17, 19,
		22, 20, 21, 23, 20, 22
	};

	//index buffer creation. Same as vertex buffer but flags are different
	D3D11_BUFFER_DESC indexDesc;
	ZeroMemory(&indexDesc, sizeof(indexDesc));
	indexDesc.Usage = D3D11_USAGE_DEFAULT;
	indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexDesc.ByteWidth = sizeof(WORD) * 36;
	indexDesc.CPUAccessFlags = 0;
	resourceData.pSysMem = indices;

	d3dResult = d3dDevice_->CreateBuffer(&indexDesc, &resourceData, &indexBuffer_);

	if (FAILED(d3dResult)) {
		DXTRACE_MSG("Failed to create index buffer");
		return false;
	}

	//Creating texture....


	//Create shader resource view for tex

	//Similar to load texture from file
	//Loads a tex and creates a shader resource view in one call
	//Params: direct3D device, path and file name of file being loaded, optional: image info struct, used for thread pump - loading via multithreading
	//out addres of shader resource being created, pointer to return value of thread pump, if not null must be valid memory location
	//Image info struct allows control of how tex image is loaded, specify values for width, height, CPU access flags etc
	d3dResult = D3DX11CreateShaderResourceViewFromFile(d3dDevice_, "decal.dds", 0, 0, &colorMap_, 0);

	if (FAILED(d3dResult)) {
		DXTRACE_MSG("Failed to load the texture image!");
		return false;
	}

	    d3dResult = D3DX11CreateShaderResourceViewFromFile( d3dDevice_,
        "decal2.dds", 0, 0, &secondMap_, 0 );

    if( FAILED( d3dResult ) )
    {
        DXTRACE_MSG( "Failed to load the second texture image!" );
        return false;
    }

	//Creating sampler state desc
	D3D11_SAMPLER_DESC colorMapDesc;
	ZeroMemory(&colorMapDesc, sizeof(colorMapDesc));

	//Used for texture address modes. Tex coordinates are specified within range of 0.0 to 1.0 for each dimension of texture
	//Tex address mode tells direct3D how to handle values outside of range
	//Are different values... Page 129
	//WRAP cause tex to wrap around and repeat
	colorMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP; //X pos of texel 
	colorMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP; //Y pos of texel 
	colorMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	//Value used for the comparison versions of tex filtering flags. Specified here
	//are flags stating comparison should pass if one value is <, ==, > etc than another value
	colorMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	colorMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR; //specifies how textue being sampled will be filtered. Are various types - Page 128
														//Refers to way values are read and combined from source and made available to shader
														//Can improve quality at cost of more expesnive tex sampling as filtering types can
														//cause more than 1 value to be read and combined into single color value that shader sees

	//Level of detail bias for MIP. Is an offset to the MIP level to use by Direct3D
	colorMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

	//Creating sampler state
	//Params: sampler description, out address to sampler state object that will store results
	d3dResult = d3dDevice_->CreateSamplerState(&colorMapDesc, &colorMapSampler_);

	if (FAILED(d3dResult)) {
		DXTRACE_MSG("Failed to create color map sampler state");
	}

	viewMatrix_ = XMMatrixIdentity();
	projMatrix_ = XMMatrixPerspectiveFovLH(XM_PIDIV4, 800.0F / 600.0F, 0.01F, 100.0f);


	return true;

}

void MultiTextureDemo::Update(float dt)
{
	//Nothing to update
}

void MultiTextureDemo::Render() {

	if (d3dContext_ == 0)

		return;

	//Clear render targets
	float clearColor[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
	d3dContext_->ClearRenderTargetView(backBufferTarget_, clearColor);
	d3dContext_->ClearDepthStencilView(depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);

	unsigned int stride = sizeof(VertexPos);
	unsigned int offset = 0;

	//set up input assembler
	//Doesn't move so don't have to clear render target but it is a good habit

	d3dContext_->IASetInputLayout(inputLayout_); //bind input assembler to input layour object created using create input functions
												 //Done each time we are about to render geometry that uses a specific input layout. Takes one param.

												 //Provide vertex buffer we are drawing out of. Sets the vertex buffer
												 //Params: starting slot to bind the buffer - first buffer in array of buffers, number of buffers being set, array of one or more buffers being set
	d3dContext_->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);

	d3dContext_->IASetIndexBuffer(indexBuffer_, DXGI_FORMAT_R16_UINT, 0);

	d3dContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST); //Set topology to triangle strip. [Change to list for square]

																				 //Input assembler set

																				 //Can set shaders now

																				 //Both of these take the shader being set, pointer to array of class instances, total number of class instances as parameters

	XMMATRIX rotationMat = XMMatrixRotationRollPitchYaw(0.0f, 0.7f, 0.7f);
	XMMATRIX translationMat = XMMatrixTranslation(0.0f, 0.0f, 6.0f);

	XMMATRIX worldMat = rotationMat * translationMat;

	ID3DX11EffectShaderResourceVariable* colorMap;
	colorMap = effect_->GetVariableByName("colorMap")->AsShaderResource();
	colorMap->SetResource(colorMap_);

	ID3DX11EffectShaderResourceVariable* colorMap2;
	colorMap2 = effect_->GetVariableByName("secondMap")->AsShaderResource();
	colorMap2->SetResource(secondMap_);

	ID3DX11EffectSamplerVariable* colorMapSampler;
	colorMapSampler = effect_->GetVariableByName("colorSampler")->AsSampler();
	colorMapSampler->SetSampler(0, colorMapSampler_);

	ID3DX11EffectMatrixVariable* worldMatrix;
	worldMatrix = effect_->GetVariableByName("worldMatrix")->AsMatrix();
	worldMatrix->SetMatrix((float*)&worldMat);

	ID3DX11EffectMatrixVariable* viewMatrix;
	viewMatrix = effect_->GetVariableByName("viewMatrix")->AsMatrix();
	viewMatrix->SetMatrix((float*)&viewMatrix_);

	ID3DX11EffectMatrixVariable* projMatrix;
	projMatrix = effect_->GetVariableByName("projMatrix")->AsMatrix();
	projMatrix->SetMatrix((float*)&projMatrix_);

	ID3DX11EffectTechnique* colorInvTechnique;
	colorInvTechnique = effect_->GetTechniqueByName("MultiTexture");

	D3DX11_TECHNIQUE_DESC techDesc;
	colorInvTechnique->GetDesc(&techDesc);

	for (unsigned int p = 0; p < techDesc.Passes; p++)
	{
		ID3DX11EffectPass* pass = colorInvTechnique->GetPassByIndex(p);

		if (pass != 0)
		{
			pass->Apply(0, d3dContext_);
			d3dContext_->DrawIndexed(36, 0, 0);
		}
	}
	


	//Present rendered geometry to the screen
	//Params: sync interval and presentation flags
	//Sync interval = 0 = present immediately or a value that states after which vertical blank we want to present e.g. 3 = third vertical blank
	//Flags can be any of the DXGI_PRESENT values where 0 means to present a frame from each buffer to the output while using verticl blank synchronisation
	//DO_NOT_SEQUENCE - present a frame from each buffer to output while usin vertical blank....
	//TEST - now present to the output - good for error checking
	//RESTART - tell drive to discard any outstanding request to present
	swapChain_->Present(0, 0);


}
