#pragma once

#include "DX11DemoBase.h"

class TriangleDemo : public Dx11DemoBase {

	//SEE BlankDemo AND DX11DemoBase for commented explanation
public:
	TriangleDemo();
	virtual ~TriangleDemo();

	bool LoadContent();
	void UnLoadContent();

	void Update(float dt);
	void Render();

private:
	//Creates a vertex and pixel shader
	//These are a base requirement in D3D for rendering ANY geometry
	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;

	//Creates input layout and a buffer 

	//inout layout describes vertext layout used by buffer
	ID3D11InputLayout* inputLayout_;
	ID3D11InputLayout* inputLayout2_;

	//Vertex buffer used all vertex data for a mesh 

	//Creates object that resides in optimal location of memory e.g. video memory of graphics hardware which is chosen by device driver
	//When renders, transfers info across graphics bus - performs ops to see if geometry is visible or not
	//Attempting to render non visible geometry leads to performance issues
	//Most commercial games determine if geometry is visible before and only send visible or potentially visible to be rendered
	ID3D11Buffer* vertexBuffer_;


};
