#pragma once

#ifndef _CUBE_DEMO_H_
#define _CUBE_DEMO_H_

#include "DX11DemoBase.h"
#include "LookAtCamera.h"
#include<xnamath.h>

class CameraDemo: public Dx11DemoBase {

public:
	CameraDemo();
	virtual ~CameraDemo();

	bool LoadContent();
	void UnLoadContent();

	void Update(float dt);
	void Render();


private:

	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;
	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;
	//Index buffer used to specify array indices into a vertex list to specify what vertices form triangles.
	//Saves memory  for larger models
	ID3D11Buffer* indexBuffer_;

	//Object shaders use to access resources
	//When load tex into memory, must use it to access data via shader, will be binding to IA
	//CAN also provide general purpose data for parallel computing and other things
	//Allows us to view buffer in shader
	ID3D11ShaderResourceView* colorMap_;

	//Can access sampling state info of a tex
	//Allows us to set properties like tex filtering and addressing
	ID3D11SamplerState* colorMapSampler_;

	//Store these in class level as they never change, aren't recalcuated for each frame
	ID3D11Buffer* viewCB_;
	ID3D11Buffer* projCB_;
	ID3D11Buffer* worldCB_;

	XMMATRIX projMatrix_;

	//Add stationary camera to the demo
	LookAtCamera camera_;


};

#endif