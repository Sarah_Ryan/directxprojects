#pragma once
#include <xnamath.h>

//A LookAtCamera is a fixed position camera.
//It is given a set position that does not change.
//The LookAtCamera needs a position, target and an up direction (usually 0, 1, 0)

class LookAtCamera {

public:
	//Initialises vectors to all be zero except the up direction which is 1
	LookAtCamera(); //Gives the same view in al lthe demos so far
	LookAtCamera(XMFLOAT3 pos, XMFLOAT3 target); //Set camera to position and target parameters

	void SetPositions(XMFLOAT3 pos, XMFLOAT3 target);
	//Calls MatrixLookAtLH, with position, target and up member vectors to create new matrix
	XMMATRIX GetViewMatrix();

private:
	XMFLOAT3 position_;
	XMFLOAT3 target_;
	XMFLOAT3 up_;
};
