#include"BlankDemo.h"
#include<xnamath.h>

//Mostly empty functions as program just clears screen using D3D

BlankDemo::BlankDemo()
{

}


BlankDemo::~BlankDemo()
{

}


bool BlankDemo::LoadContent()
{
	return true;
}


void BlankDemo::UnloadContent()
{

}


void BlankDemo::Update(float dt)
{

}

void BlankDemo::Render() {

	if (d3dContext_ == 0) {
		return;
	}

	float clearColor[4] = { 50.0f, 0.0f, 0.0f,1.0f };
	//Params: target to clear and value to clear it to
	//clear screen to specific colour
	d3dContext_->ClearRenderTargetView(backBufferTarget_, clearColor);

	//draw geometry 

	//Params: sync interval, presentation flags
	//sync interval can be 0 for immediate display. OR 1,2,3 or 4 to present after the nth vertical blank. Vertical blank is time difference between last line of frame update
	//for current frame and first line update of next frame
	//flags set to 0 to output to each buffer. Present test for testing  or do not sequence for presenting without sequencing to exploit vertical blank synchronisation 
	//Present newly rendered screen with cleared colour
	swapChain_->Present(0, 0);

}