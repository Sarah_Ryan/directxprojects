
#ifndef _DEMO_BASE_H_
#define _DEMO_BASE_H_


#include<d3d11.h>
#include<d3dx11.h>
#include<DxErr.h>
#include <dinput.h>

#define KEYDOWN(name, key)(name[key] & 0x80)

class Dx11DemoBase
{
public:
	//Constructer sets values to null to start
	Dx11DemoBase();
	virtual ~Dx11DemoBase();

	bool Initialize(HINSTANCE hInstance, HWND hwnd);
	bool CompileD3DShader(char* filepath, char* entry, char* shaderModel, ID3DBlob** buffer);

	//Ensure objects are released
	void Shutdown();

	//Loads project/demo specific objects e.g. geometry, texture images, shaders, audio etc
	virtual bool LoadContent();

	//Releases any project/demo specific objects
	virtual void UnloadContent();

	virtual void Update(float dt) = 0;
	virtual void Render() = 0;

protected:
	HINSTANCE hInstance_;
	HWND hwnd_;

	D3D_DRIVER_TYPE driverType_;
	D3D_FEATURE_LEVEL featureLevel_;

	//Minimum D3D objects
	ID3D11Device* d3dDevice_; //Device is the device itself and communicates with hardware
	ID3D11DeviceContext* d3dContext_; //Rendering context that tells device how to draw. Includes rendering states and drawing info
	IDXGISwapChain* swapChain_; //Rendering destinations that device and context will draw to
	ID3D11RenderTargetView* backBufferTarget_; //Need render target view so output merger can render to the swap chain back buffer

	//Object for direct input
	LPDIRECTINPUT8 directInput_;
	//Object for direct input keyboard
	LPDIRECTINPUTDEVICE8 keyboardDevice_;

	//2 arrays to store the current and previous state of all keyboard keys
	char keyboardKeys_[256];
	char prevKeyboardKeys_[256];

};

#endif