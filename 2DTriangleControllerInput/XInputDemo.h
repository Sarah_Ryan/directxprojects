#pragma once

#ifndef _KEYBOARD_DEMO_H_
#define _KEYBOARD_DEMO_H_

#include "DX11DemoBase.h"
#include<XInput.h>

class XInputDemo : public Dx11DemoBase {

	//SEE BlankDemo AND DX11DemoBase for commented explanation
public:
	XInputDemo();
	virtual ~XInputDemo();

	bool LoadContent();
	void UnLoadContent();

	void Update(float dt);
	void Render();

private:
	//Creates a vertex and pixel shader
	//These are a base requirement in D3D for rendering ANY geometry
	ID3D11VertexShader* customColorVS_;
	ID3D11PixelShader* customColorPS_;

	//Creates input layout and a buffer 

	//inout layout describes vertext layout used by buffer
	ID3D11InputLayout* inputLayout_;

	//Vertex buffer used all vertex data for a mesh 

	//Creates object that resides in optimal location of memory e.g. video memory of graphics hardware which is chosen by device driver
	//When renders, transfers info across graphics bus - performs ops to see if geometry is visible or not
	//Attempting to render non visible geometry leads to performance issues
	//Most commercial games determine if geometry is visible before and only send visible or potentially visible to be rendered
	ID3D11Buffer* vertexBuffer_;

	//Use a constant buffer to store color value that we want to shade surface with
	//Is created in load content
	ID3D11Buffer* colorCB_;
	int selectedColor_ = 0;

	XINPUT_STATE controllerState_;
	XINPUT_STATE prevControllerState_;

};

#endif
