#include "MouseDemo.h"
#include <xnamath.h>


struct VertexPos {

	//Struct with X, Y, Z floating point values within
	//XM refers to XNA math
	//Float refers to data type of members
	//3 refers to how many numbers the structure has
	XMFLOAT3 pos;

};


MouseDemo::MouseDemo() : customColorVS_(0), customColorPS_(0), inputLayout_(0), vertexBuffer_(0) {



}

MouseDemo::~MouseDemo() {


}

void MouseDemo::UnLoadContent() {

	//releases memory used by objects
	if (customColorVS_) customColorVS_->Release();
	if (customColorPS_) customColorPS_->Release();
	if (inputLayout_) inputLayout_->Release();
	if (vertexBuffer_) vertexBuffer_->Release();
	if (colorCB_) colorCB_->Release();

	colorCB_ = 0;
	customColorVS_ = 0;
	customColorPS_ = 0;
	inputLayout_ = 0;
	vertexBuffer_ = 0;
	
}

bool MouseDemo::LoadContent() {

	ID3DBlob* vsBuffer = 0;

	//load vertex shader from file - name because shaders work together to shade surface with solid green colour
	//Load vetex from text file, compile into byte code OR DX11 will do for us during startup

	//This function call is the same as D3DX11CompileFromFile, function is written in DemoBase cpp
	//Params: Path of the HLSL shader code to be loaded and compiled, global macros within shaders code (N/A),
				//Macros work in HLSL same as they work in C/C++ - Defined in app side use type shader macro 
	//optional param for handling include statements that exist in HLSL file - used to specify behaviour for opening/closing files included in shader source (N/A),
	//Function name for shader you are compiling. File can have shader types and functions - this param points to entry point of shader we are compiling,
	//specifies shader model (4.0 = DX10 and above), (5.0 = DX11 hardware and above), memory address shader will reside

	//There are more parameters for the CompileFromFile function but we aren't using them. Can look up if needed.
	bool compileResult = CompileD3DShader("CustomColor.fx", "VS_Main", "vs_4_0", &vsBuffer);

	//Error handling
	if (compileResult == false) {
		MessageBox(0, "Error loading vertex shader", "Compile Error", MB_OK);
		return false;
	}

	HRESULT d3dResult;

	//Create vertex shader after compiled from source
	//Params: buffer for compiled code, its size in bytes, pointer to class linkage type, pointer to vertex shader object we are creating
	d3dResult = d3dDevice_->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), 0, &customColorVS_);

	if (FAILED(d3dResult)) {
		if (vsBuffer)
			vsBuffer->Release();

		return false;

	}

	//Create vertex layout
	//Vertex layout is validated against vertex shader signature, we need at least vertex shader loaded into memory
	//Describe vertex layout of a vertex structure
		D3D11_INPUT_ELEMENT_DESC solidColorLayout[] = 
		{
				//First member - Semantic name that describes purpose of element
				//POSITION = Used for vertex's position. 
				//Other examples: COLOR, NORMAL etc
				//Semantic binds element to HLSL shader's input/output variable
				{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0}

		};
	
		unsigned int totalLayoutElements = ARRAYSIZE(solidColorLayout);

		//Input layout created
		//Params: array of elements in vertex layout, number of elements in array, 
		//compiled vertex shader code with input signature that will be validated against array of elements,
		//size of shader's byte code, AND pointer of the object that will be created with this function call
		//Input signature MUST match input layout or function call will fail
		d3dResult = d3dDevice_->CreateInputLayout(solidColorLayout, totalLayoutElements, vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &inputLayout_);

			vsBuffer->Release();

			if (FAILED(d3dResult)) {
				return false;
			}

			//Load pixel shader because this is needed for DX10 and 11
			ID3DBlob* psBuffer = 0;
			compileResult = CompileD3DShader("CustomColor.fx", "PS_Main", "ps_4_0", &psBuffer);

			if (compileResult == false) {
				MessageBox(0, "Error loading pixel shader!", "Compile Error", MB_OK);
				return false;
			}
			d3dResult = d3dDevice_->CreatePixelShader(psBuffer->GetBufferPointer(), psBuffer->GetBufferSize(), 0, &customColorPS_);

			psBuffer->Release();

			if (FAILED(d3dResult)) {
				return false;
			}

			//Triangle, half unit in size along X and Y
			//Also 0.5 along Z so that it will be visible on screen.
			//Won't render if cam is too close or behind surface
			VertexPos vertices[] = {

				//X,Y,Z for each vertex
				XMFLOAT3(0.5f, 0.5f, 0.5f), //top right
				XMFLOAT3(0.5f, -0.5f, 0.5f), //bottom right
				XMFLOAT3(-0.5f, -0.5f, 0.5f), //bottom left
				//XMFLOAT3(-0.5f, 0.5f, 0.5f), //top left

				//Need this one or DX will draw straight back down to te bottom right 
				//Will get a shape that looks like it has a triangle cut out of it
				//XMFLOAT3(0.5f, 0.5f, 0.5f), //top right

		


			};



			//Above vertex list stored in array (vertices) and provided as a sub resource data 
			//Used during CreateBuffer call to create actual vertex buffer

			//Vertex buffer setup

			//Create buffer description
			//Used to provide details of the buffer we are creating, important as technically could be something other than vertex buffer
			D3D11_BUFFER_DESC vertexDesc;
			ZeroMemory(&vertexDesc, sizeof(vertexDesc));

			vertexDesc.Usage = D3D11_USAGE_DEFAULT;
			vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexDesc.ByteWidth = sizeof(VertexPos) * 3;

			//Subresource creation
			//Used (in this case) to pass vertex data to buffers creation function so buffer is filled
			//Can just pass NULL into creation = empty buffer. But we know what we want to store in it
			D3D11_SUBRESOURCE_DATA resourceData;
			ZeroMemory(&resourceData, sizeof(resourceData));

			// is pointer to initialized memory OR in our case, memory we are sending to fill buffer with
			resourceData.pSysMem = vertices;

			//create actual vertex buffer
			//Params: buffer description, optional sub resource data and pointer for buffer object, defined by descriptor
			d3dResult = d3dDevice_->CreateBuffer(&vertexDesc, &resourceData, &vertexBuffer_);

			if (FAILED(d3dResult)) {
				return false;
			}

			//if succeeds, we can draw geometry in the buffer at any point afterwards

			//Constant buffer setup
			//We are leaving the buffer empty as we are going to fill it in during render function

			D3D11_BUFFER_DESC constDesc;
			ZeroMemory(&constDesc, sizeof(constDesc));
			constDesc.Usage = D3D11_USAGE_DEFAULT;
			//CB Buffer created by setting these flags
			constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			constDesc.ByteWidth = sizeof(XMFLOAT4);
			

			d3dResult = d3dDevice_->CreateBuffer(&constDesc, 0, &colorCB_);

			if (FAILED(d3dResult)) {
				return false;
			}

			return true;

}

void MouseDemo::Update(float dt)
{
	//Used to read input. Function is same regardless of device.
	//Params: DWORD value that holds size of buffer being passed as second param, pointer to the buffer that will hold data read from device. Format defined in DemoBase -> Initialise
	keyboardDevice_->GetDeviceState(sizeof(keyboardKeys_), (LPVOID)&keyboardKeys_);

	mouseDevice_->GetDeviceState(sizeof(mouseState_), (LPVOID)&mouseState_);

	//Button press event for escape key
	if (GetAsyncKeyState(VK_ESCAPE)) {
		PostQuitMessage(0);
	}

	//When up/down arrow is pressed, it will cycle through 3 colors
	//Only counts when button is released otherwise the colors would cycle rapidly
	//0 = red, 1 = green and 2 = blue
	//KEYDOWN macro returns true or false depending if key is pressed or not

	//Button up event for the down arrow key
	//If previous button state was down and current is not down then it has been pressed
	if (KEYDOWN(prevKeyboardKeys_, DIK_DOWN) && !KEYDOWN(keyboardKeys_, DIK_DOWN)) {
		selectedColor_--;
	}

	//Button up event for the up arrow key
	//Same logic as for down arrow key
	if(KEYDOWN(prevKeyboardKeys_, DIK_UP) && !KEYDOWN(keyboardKeys_, DIK_UP)) {
		selectedColor_++;
	}

	if (BUTTONDOWN(mouseState_, 0) && !BUTTONDOWN(prevMouseState_, 0)) {
		selectedColor_++;
	}

	if (BUTTONDOWN(mouseState_, 1) && !BUTTONDOWN(prevMouseState_, 1)) {
		selectedColor_--;
	}

	//X, Y, Z values hold relative position to where mouse previously was
	//To track correctly  you must keep track of values read from mouse on the previous frame to interpret movement correctly
	//Example:
	mousePosX_ += mouseState_.lX; 
	mousePosY_ += mouseState_.lY; 
	//Will stay 0 if there is no wheel
	mouseWheel_ += mouseState_.lZ;

	memcpy(prevKeyboardKeys_, keyboardKeys_, sizeof(keyboardKeys_));
	memcpy(&prevMouseState_, &mouseState_, sizeof(mouseState_));

	//Ensures that the selectedColor variable stays in the range of 0 - 2
	if (selectedColor_ < 0) selectedColor_ = 2;
	if (selectedColor_ > 2) selectedColor_ = 0;
}

void MouseDemo::Render() {

	if (d3dContext_ == 0)

		return;

	//Clear render targets
	float clearColor[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
	d3dContext_->ClearRenderTargetView(backBufferTarget_, clearColor);

	unsigned int stride = sizeof(VertexPos);
	unsigned int offset = 0;

	//set up input assembler
	//Doesn't move so don't have to clear render target but it is a good habit

	d3dContext_->IASetInputLayout(inputLayout_); //bind input assembler to input layour object created using create input functions
												//Done each time we are about to render geometry that uses a specific input layout. Takes one param.

	//Provide vertex buffer we are drawing out of. Sets the vertex buffer
	//Params: starting slot to bind the buffer - first buffer in array of buffers, number of buffers being set, array of one or more buffers being set
	d3dContext_->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset); 

	d3dContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST); //Set topology to triangle LIST. [Change to STRIP for square]

	//Sets RGBA values for each color
	XMFLOAT4 redColor(1.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4 greenColor(0.0f, 1.0f, 0.0f, 1.0f);
	XMFLOAT4 blueColor(0.0f, 0.0f, 1.0f, 1.0f);

	//Depending on what number selectedColor is, fill constant buffer with correct color value
	if (selectedColor_ == 0) {
		d3dContext_->UpdateSubresource(colorCB_, 0, 0, &redColor, 0, 0);
	}

	else if (selectedColor_ == 1) {
		d3dContext_->UpdateSubresource(colorCB_, 0, 0, &greenColor, 0, 0);
		//MessageBox(0, "Green!", 0, MB_OK);
	}

	else {
		d3dContext_->UpdateSubresource(colorCB_, 0, 0, &blueColor, 0, 0);
		//MessageBox(0, "Blue!", 0, MB_OK);
	}

	//Input assembler set

	//Can set shaders now

	//Both of these take the shader being set, pointer to array of class instances, total number of class instances as parameters
	d3dContext_->VSSetShader(customColorVS_, 0, 0);
	d3dContext_->PSSetShader(customColorPS_, 0, 0);
	d3dContext_->PSSetConstantBuffers(0, 1, &colorCB_);


	//After set and bound all data for geometry - Call draw
	//Parans: total number of vertices in vertex array, start vertex location - can act as offset into vertex buffer where you wish to begin drawing
	d3dContext_->Draw(3, 0);
	

	//Present rendered geometry to the screen
	//Params: sync interval and presentation flags
	//Sync interval = 0 = present immediately or a value that states after which vertical blank we want to present e.g. 3 = third vertical blank
	//Flags can be any of the DXGI_PRESENT values where 0 means to present a frame from each buffer to the output while using verticl blank synchronisation
		//DO_NOT_SEQUENCE - present a frame from each buffer to output while usin vertical blank....
		//TEST - now present to the output - good for error checking
		//RESTART - tell drive to discard any outstanding request to present
	swapChain_->Present(0, 0);


}
