#pragma once

#ifndef _TRIANGLE_OBJECT_H_
#define _TRIANGLE_OBJECT_H_


#include<xnamath.h>

class TriangleObject {

public:
	//Explanations in older demo projects
	TriangleObject();
	virtual ~TriangleObject();

	XMMATRIX GetWorldMatrix();

	void SetPosition(XMFLOAT2& position);
	void SetRotation(float rotation);
	void SetScale(XMFLOAT2& scale);


private:
	//Variables for matrices
	XMFLOAT2 position_;
	float rotation_;
	XMFLOAT2 scale_;
};

#endif