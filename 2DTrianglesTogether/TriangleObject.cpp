#include<d3d11.h>
#include <D3DX11.h>
#include "TriangleObject.h"

//Sets up way to store an individual sprites position, rotation and scale
//As well as a way to build Sprite's world (model) view


TriangleObject::TriangleObject() : rotation_(0) {
	//Scale is set to 1 fr both axis...
	//because anything less will shrink, anything more will make it bigger
	//1 keeps sprites size unchanged
	scale_.x = scale_.y = 1.0f;
}

TriangleObject::~TriangleObject() {

}

XMMATRIX TriangleObject::GetWorldMatrix() {

	//Params: X, Y and Z values
	XMMATRIX translation = XMMatrixTranslation(position_.x, position_.y, 0.0f);

	XMMATRIX rotationZ = XMMatrixRotationZ(rotation_);
	XMMATRIX scale = XMMatrixScaling(scale_.x, scale_.y, 1.0f);

	return translation * rotationZ * scale;
};

void TriangleObject::SetPosition(XMFLOAT2& position) {
	position_ = position;
}

void TriangleObject::SetRotation(float rotation) {
	rotation_ = rotation;
}

void TriangleObject::SetScale(XMFLOAT2& scale) {
	scale_ = scale;
}

