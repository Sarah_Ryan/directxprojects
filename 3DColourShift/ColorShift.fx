/*
    Beginning DirectX 11 Game Programming
    By Allen Sherrod and Wendy Jones

    Texture Mapping Shader
*/

//New global objects


//To bind objects to shader inputs we are using in the rendering function, must register with  HLSL
//Since calls in the render function can take in arrays, we must specify index - 0
Texture2D colorMap : register(t0); //Type of texture2D as it is being used for 2D texture
SamplerState colorSampler : register( s0 ); //HLSL type of sampler state


cbuffer cbChangesEveryFrame : register(b0) 
{
	matrix worldMatrix;
};

cbuffer cbNeverChanges : register (b1) 
{
	matrix viewMatrix;
};

cbuffer cbChangesOnResize : register(b2) 
{
	matrix projMatrix;
};

struct VS_Input
{
    float4 pos  : POSITION;
    //Allows for texture coordinates
   
    float2 tex0 : TEXCOORD0;
};

struct PS_Input
{
    float4 pos  : SV_POSITION;
    //Allows for texture coordinates
    float2 tex0 : TEXCOORD0;
};


PS_Input VS_Main( VS_Input vertex )
{
    // Takes coordinates from vertex buffer and passes into pixel buffer to use
    PS_Input vsOut = ( PS_Input )0;
	vsOut.pos = mul(vertex.pos, worldMatrix);
	vsOut.pos = mul(vsOut.pos, viewMatrix);
	vsOut.pos = mul(vsOut.pos, projMatrix);
    vsOut.tex0 = vertex.tex0;

    return vsOut;
}


float4 PS_Main(PS_Input frag) : SV_TARGET //Target semantic specifies that the PS output should be used for rendering target
{
	//1 minus the colour of the cube. 
	//This will invert the colour
	
	//Put objects colour into finalCol
	float4 col = colorMap.Sample(colorSampler, frag.tex0);
	float4 finalCol;

	//Store shifted values
	//Red = Blue, Blue = Green and Green = Red
	finalCol.x = col.y;
	finalCol.y = col.z;
	finalCol.z = col.x;
	finalCol.w = 1.0f;

	return finalCol;
}

technique11 ColorShift {
	pass P0 {
		SetVertexShader(CompileShader(vs_5_0, VS_Main()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS_Main()));
	}
}