/*
    Beginning DirectX 11 Game Programming
    By Allen Sherrod and Wendy Jones

    Texture Mapping Shader
*/

//New global objects


//To bind objects to shader inputs we are using in the rendering function, must register with  HLSL
//Since calls in the render function can take in arrays, we must specify index - 0
Texture2D colorMap_ : register(t0); //Type of texture2D as it is being used for 2D texture
SamplerState colorSampler_ : register( s0 ); //HLSL type of sampler state


struct VS_Input
{
    float4 pos  : POSITION;
    //Allows for texture coordinates
   
    float2 tex0 : TEXCOORD0;
};

struct PS_Input
{
    float4 pos  : SV_POSITION;
    //Allows for texture coordinates
    float2 tex0 : TEXCOORD0;
};


PS_Input VS_Main( VS_Input vertex )
{
    // Takes coordinates from vertex buffer and passes into pixel buffer to use
    PS_Input vsOut = ( PS_Input )0;
    vsOut.pos = vertex.pos;
    vsOut.tex0 = vertex.tex0;

    return vsOut;
}


float4 PS_Main( PS_Input frag ) : SV_TARGET
{
    //Uses tex coordinate and texture object to read color value
    //Done by calling sample function. Params: sampler state object and a pair of texture coordinates, must be of type float2 for 2D texture
    return colorMap_.Sample( colorSampler_, frag.tex0 );
}