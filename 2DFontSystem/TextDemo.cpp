#include "TextDemo.h"
#include <xnamath.h>


struct VertexPos {

	//Struct with X, Y, Z floating point values within
	//XM refers to XNA math
	//Float refers to data type of members
	//3 refers to how many numbers the structure has
	XMFLOAT3 pos;

	//Performing texture mapping....
	//Need to update vertex structure to including 2 floating point variables for TU and TV tex coordinates
	XMFLOAT2 tex0;

};


TextDemo::TextDemo() : solidColorVS_(0), solidColorPS_(0), inputLayout_(0), vertexBuffer_(0) {


	 
}

TextDemo::~TextDemo() {


}

void TextDemo::UnLoadContent() {

	//releases memory used by objects
	if (colorMapSampler_) colorMapSampler_->Release();
	if (colorMap_) colorMap_->Release();
	if (solidColorVS_) solidColorVS_->Release();
	if (solidColorPS_) solidColorPS_->Release();
	if (inputLayout_) inputLayout_->Release();
	if (vertexBuffer_) vertexBuffer_->Release();

	colorMapSampler_ = 0;
	colorMap_ = 0;
	solidColorVS_ = 0;
	solidColorPS_ = 0;
	inputLayout_ = 0;
	vertexBuffer_ = 0;
}

bool TextDemo::DrawString(char* message, float startX, float startY) {
	//If string is longer than 24 letters, will clamp at 24.
	//size in bytes for single sprite

	const int sizeOfSprite = sizeof(VertexPos) * 6;

	//Dynamic buffer set up for max of 24 letters
	const int maxLetters = 24;

	//Length of string we are attempting to render
	int length = strlen(message);

	//clamp for too long strings
	if (length > maxLetters) {
		length = maxLetters;
	}

	//Char's width on screen - Width of individual letter in screen coordinates
		float charWidth = 32.0f / 800.0f;

	//Char's height on screen -	Height of individual letter in screen coordinates
		float charHeight = 32.0f / 640.0f;

	//Char's texel width - Width of individual letter in texture image
		float texelWidth = 32.0f / 864.0f;

	//verts pre triangle (3) * total triangles (2) = 6
	//Constant storing total vertices in each sprite
		const int verticesPerLeter = 6;

		
		D3D11_MAPPED_SUBRESOURCE mapResource;
		//To update contents of dynamic buffer we need to call the map function!
		//Params; buffer we are mapping, sub resource index (0 as only 1), the mapping type, maps flags and mapped resource to hold data
		//Mapping type here tells D3D that previous values in buffer should be considered undefined
		HRESULT d3dResult = d3dContext_->Map(vertexBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapResource);

		if (FAILED(d3dResult)) {
			DXTRACE_MSG("Failed to map resource");
			return false;
		}

		//Once mapping completed, will have access to buffer's contents via mapResource
		//To update buffer we can copy data we want to mapResource pdata member...

		//Point to vertex buffer's internal data
		VertexPos *spritePtr = (VertexPos*)mapResource.pData;

		//Set up values to give us ASCII of letters A and Z which will be used in loop to generate string geometry
		const int indexA = static_cast<char>('A');
		const int indezZ = static_cast<char>('Z');

		//for loop iterates over entire string
		//Sets X and Y values for each character
		//StartX var is the starting X of the first character
		//As we loop, we want to add the width of a character to this starting position to get the next
		//Using loop index, we can multiply that by size of character's width and add to starting pos to get current position
		//Don't have to keep track of previous X as we can calculate
		for (int i = 0; i < length; i++) {

			//Is left position of the sprite
			float thisStartX = startX + (charWidth * static_cast<float>(i));
			//EndX is right side of character which is start X and size of 1 character
			float thisEndX = thisStartX + charWidth;
			float thisEndY = startY + charHeight;

			//set vertex positions
			spritePtr[0].pos = XMFLOAT3(thisEndX, thisEndY, 1.0f);
			spritePtr[1].pos = XMFLOAT3(thisEndX, startY, 1.0f);
			spritePtr[2].pos = XMFLOAT3(thisStartX, startY, 1.0f);
			spritePtr[3].pos = XMFLOAT3(thisStartX, startY, 1.0f);
			spritePtr[4].pos = XMFLOAT3(thisStartX, thisEndY, 1.0f);
			spritePtr[5].pos = XMFLOAT3(thisEndX, thisEndY, 1.0f);


			//instead of using a loop index we use the letter itself tto set tex coordinates generation
			//Done by calculating the ASCII value of letter
			//First letter in image is A, subtract current ASCII value by that of A, get 0 is A, 1 if B etc 
			//Use that value, stored in tex lookup. Starts at 0 and increments
			int texLookup = 0;
			int letter = static_cast<char>(message[i]);

			//If invalid character....
			if (letter < indexA || letter > indezZ) {

				//set it to last character in the font.dds which is 32 x 32 blank space
				texLookup = (indezZ - indexA) + 1;
			}
			else {
				//A = 0, B = 1, Z = 25 etc
				texLookup = (letter - indexA);
			}

			//Coordinates range from 0.0 to 1.0
			//Starting TU always 0 for first letter
			float tuStart = 0.0f + (texelWidth* static_cast<float>(texLookup));
			float tuEnd = tuStart + texelWidth;

			spritePtr[0].tex0 = XMFLOAT2(tuEnd, 0.0f);
			spritePtr[1].tex0 = XMFLOAT2(tuEnd, 1.0f);
			spritePtr[2].tex0 = XMFLOAT2(tuStart, 1.0f);
			spritePtr[3].tex0 = XMFLOAT2(tuStart, 1.0f);
			spritePtr[4].tex0 = XMFLOAT2(tuStart, 0.0f);
			spritePtr[5].tex0 = XMFLOAT2(tuEnd, 0.0f);

			spritePtr += 6;
		}

		//Have to unmap vertx buffer. Must be done for all mapped resources
		//Params: buffer to unmap and subresource index
		d3dContext_->Unmap(vertexBuffer_, 0);
		//draw displays results
		//Takes 6 * length because each sprite has 6 vertices and length is total number of sprites
		d3dContext_->Draw(6 * length, 0);

		return true;
	
}

bool TextDemo::LoadContent() {

	ID3DBlob* vsBuffer = 0;

	//load vertex shader from file - name because shaders work together to shade surface with solid green colour
	//Load vetex from text file, compile into byte code OR DX11 will do for us during startup

	//This function call is the same as D3DX11CompileFromFile, function is written in DemoBase cpp
	//Params: Path of the HLSL shader code to be loaded and compiled, global macros within shaders code (N/A),
	//Macros work in HLSL same as they work in C/C++ - Defined in app side use type shader macro 
	//optional param for handling include statements that exist in HLSL file - used to specify behaviour for opening/closing files included in shader source (N/A),
	//Function name for shader you are compiling. File can have shader types and functions - this param points to entry point of shader we are compiling,
	//specifies shader model (4.0 = DX10 and above), (5.0 = DX11 hardware and above), memory address shader will reside

	//There are more parameters for the CompileFromFile function but we aren't using them. Can look up if needed.
	bool compileResult = CompileD3DShader("TextureMap.fx", "VS_Main", "vs_4_0", &vsBuffer);

	//Error handling
	if (compileResult == false) {
		MessageBox(0, "Error loading vertex shader", "Compile Error", MB_OK);
		return false;
	}

	HRESULT d3dResult;

	//Create vertex shader after compiled from source
	//Params: buffer for compiled code, its size in bytes, pointer to class linkage type, pointer to vertex shader object we are creating
	d3dResult = d3dDevice_->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), 0, &solidColorVS_);

	if (FAILED(d3dResult)) {
		if (vsBuffer)
			vsBuffer->Release();

		return false;

	}

	//Create vertex layout
	//Vertex layout is validated against vertex shader signature, we need at least vertex shader loaded into memory
	//Describe vertex layout of a vertex structure
	D3D11_INPUT_ELEMENT_DESC solidColorLayout[] =
	{
		//First member - Semantic name that describes purpose of element
		//POSITION = Used for vertex's position. 
		//Other examples: COLOR, NORMAL etc
		//Semantic binds element to HLSL shader's input/output variable
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		//Element for tex coordinate, semantic TEXCORD
		//R32G32 as we are only using 2 float values
		//offset is 12 as there is a float 3 in beginning for position, 
		//tex coordinates won't appear untly after 12 byte
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}

	};

	unsigned int totalLayoutElements = ARRAYSIZE(solidColorLayout);

	//Input layout created
	//Params: array of elements in vertex layout, number of elements in array, 
	//compiled vertex shader code with input signature that will be validated against array of elements,
	//size of shader's byte code, AND pointer of the object that will be created with this function call
	//Input signature MUST match input layout or function call will fail
	d3dResult = d3dDevice_->CreateInputLayout(solidColorLayout, totalLayoutElements, vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &inputLayout_);

	vsBuffer->Release();

	if (FAILED(d3dResult)) {
		return false;
	}

	//Load pixel shader because this is needed for DX10 and 11
	ID3DBlob* psBuffer = 0;
	compileResult = CompileD3DShader("TextureMap.fx", "PS_Main", "ps_4_0", &psBuffer);

	if (compileResult == false) {
		MessageBox(0, "Error loading pixel shader!", "Compile Error", MB_OK);
		return false;
	}
	d3dResult = d3dDevice_->CreatePixelShader(psBuffer->GetBufferPointer(), psBuffer->GetBufferSize(), 0, &solidColorPS_);

	psBuffer->Release();

	if (FAILED(d3dResult)) {
		return false;
	}

	//Triangle, half unit in size along X and Y
	//Also 0.5 along Z so that it will be visible on screen.
	//Won't render if cam is too close or behind surface
	//VertexPos vertices[] = {

	//	//X,Y,Z for each vertex and texture coordinates
	//	{XMFLOAT3(1.0f,1.0f,1.0f), XMFLOAT2(1.0f, 1.0f)}, //top right
	//	{XMFLOAT3(1.0f, -1.0f,1.0f), XMFLOAT2(1.0f, 0.0f)}, //bottom right
	//	{XMFLOAT3(-1.0f,-1.0f,1.0f), XMFLOAT2(0.0f, 0.0f) }, //bottom left

	//	{XMFLOAT3(-1.0f,-1.0f,1.0f), XMFLOAT2(0.0f, 0.0f) },
	//	{XMFLOAT3(-1.0f,1.0f,1.0f), XMFLOAT2(0.0f, 1.0f) },
	//	{XMFLOAT3(1.0f,1.0f,1.0f), XMFLOAT2(1.0f, 1.0f) },



	//};



	//Above vertex list stored in array (vertices) and provided as a sub resource data 
	//Used during CreateBuffer call to create actual vertex buffer

	//Vertex buffer setup

	//Create buffer description
	//Used to provide details of the buffer we are creating, important as technically could be something other than vertex buffer
	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));

	vertexDesc.Usage = D3D11_USAGE_DYNAMIC; //Allows to be dynamically updated by CPU. DEFAULT = Static buffers
	vertexDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; //Grants CPU write access to resource in GPU
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	const int sizeOfSprite = sizeof(VertexPos) * 6; //Size of single sprite in bytes. 2 triangles made up of 6 vertices
	const int maxLetters = 24; //Stores up to 24 sprites.... 24 letters

	vertexDesc.ByteWidth = sizeOfSprite * maxLetters;
	
	//Leave subresource empty as we are dynamically update buffer

	//create actual vertex buffer
	//Params: buffer description, optional sub resource data and pointer for buffer object, defined by descriptor
	d3dResult = d3dDevice_->CreateBuffer(&vertexDesc, 0, &vertexBuffer_);

	if (FAILED(d3dResult)) {
		return false;
	}

	//if succeeds, we can draw geometry in the buffer at any point afterwards

	//Creating texture....


	//Create shader resource view for tex

	//Similar to load texture from file
	//Loads a tex and creates a shader resource view in one call
	//Params: direct3D device, path and file name of file being loaded, optional: image info struct, used for thread pump - loading via multithreading
	//out addres of shader resource being created, pointer to return value of thread pump, if not null must be valid memory location
	//Image info struct allows control of how tex image is loaded, specify values for width, height, CPU access flags etc
	d3dResult = D3DX11CreateShaderResourceViewFromFile(d3dDevice_, "font.dds", 0, 0, &colorMap_, 0);

	if (FAILED(d3dResult)) {
		DXTRACE_MSG("Failed to load the texture image!");
		return false;
	}

	//Creating sampler state desc
	D3D11_SAMPLER_DESC colorMapDesc;
	ZeroMemory(&colorMapDesc, sizeof(colorMapDesc));

	//Used for texture address modes. Tex coordinates are specified within range of 0.0 to 1.0 for each dimension of texture
	//Tex address mode tells direct3D how to handle values outside of range
	//Are different values... Page 129
	//WRAP cause tex to wrap around and repeat
	colorMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP; //X pos of texel 
	colorMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP; //Y pos of texel 
	colorMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	//Value used for the comparison versions of tex filtering flags. Specified here
	//are flags stating comparison should pass if one value is <, ==, > etc than another value
	colorMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	colorMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR; //specifies how textue being sampled will be filtered. Are various types - Page 128
														//Refers to way values are read and combined from source and made available to shader
														//Can improve quality at cost of more expesnive tex sampling as filtering types can
														//cause more than 1 value to be read and combined into single color value that shader sees

	//Level of detail bias for MIP. Is an offset to the MIP level to use by Direct3D
	colorMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

	//Creating sampler state
	//Params: sampler description, out address to sampler state object that will store results
	d3dResult = d3dDevice_->CreateSamplerState(&colorMapDesc, &colorMapSampler_);

	if (FAILED(d3dResult)) {
		DXTRACE_MSG("Failed to create color map sampler state");
	}

	return true;

}

void TextDemo::Update(float dt)
{
	//Nothing to update
}

void TextDemo::Render() {

	if (d3dContext_ == 0)

		return;

	//Clear render targets - Dark blue background
	float clearColor[4] = { 0.2f, 0.22f, 0.24f, 1.0f };
	d3dContext_->ClearRenderTargetView(backBufferTarget_, clearColor);

	unsigned int stride = sizeof(VertexPos);
	unsigned int offset = 0;

	//set up input assembler
	//Doesn't move so don't have to clear render target but it is a good habit

	d3dContext_->IASetInputLayout(inputLayout_); //bind input assembler to input layour object created using create input functions
												 //Done each time we are about to render geometry that uses a specific input layout. Takes one param.

												 //Provide vertex buffer we are drawing out of. Sets the vertex buffer
												 //Params: starting slot to bind the buffer - first buffer in array of buffers, number of buffers being set, array of one or more buffers being set
	d3dContext_->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);

	d3dContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST); //Set topology to triangle strip. [Change to list for square]

																				 //Input assembler set

																				 //Can set shaders now

																				 //Both of these take the shader being set, pointer to array of class instances, total number of class instances as parameters
	d3dContext_->VSSetShader(solidColorVS_, 0, 0);
	d3dContext_->PSSetShader(solidColorPS_, 0, 0);


	//Add tes resource and sampler state...
	//These are used to set them to the pixel shader

	//Params: start slot to begin inserting resources, number of resources to insert and an array of resources to insert
	d3dContext_->PSSetShaderResources(0, 1, &colorMap_);
	//Params: start slot, number of samplers to insert and an array of samplers to insert
	d3dContext_->PSSetSamplers(0, 1, &colorMapSampler_);




	//DrawString function is called to handle generation and rendering of text message
	DrawString("MEOW", -0.2f, 0.0f);


	//Present rendered geometry to the screen
	//Params: sync interval and presentation flags
	//Sync interval = 0 = present immediately or a value that states after which vertical blank we want to present e.g. 3 = third vertical blank
	//Flags can be any of the DXGI_PRESENT values where 0 means to present a frame from each buffer to the output while using verticl blank synchronisation
	//DO_NOT_SEQUENCE - present a frame from each buffer to output while usin vertical blank....
	//TEST - now present to the output - good for error checking
	//RESTART - tell drive to discard any outstanding request to present
	swapChain_->Present(0, 0);


}
