#pragma once
#include <xnamath.h>

//ArcBallCam is good for moments in game where an object is the target and the camera needs to rotate around that target in a spherical manner

class ArcCamera {

public:
	//defaults a target at origin
	//Distance of 2 units away from the target
	//rotation restraints that total 180 degrees (-90 to 90)
	//These will be replaced with real positions
	ArcCamera(); 
				 
	void SetDistance(float distance, float minDistance, float maxDistance); //set current distance from camera as well as min and max distance limits
	void SetRotation(float x, float y, float minY, float maxY); //Sets current X and Y rotation as well as limits
	void SetTarget(XMFLOAT3& target); //Set current target position

	void ApplyZoom(float zoomDelta);
	void ApplyRotation(float yawDelta, float pitchDelta);

	//Position will be calculated here
	XMMATRIX GetViewMatrix();
	XMFLOAT3 GetPosition();

private:
	XMFLOAT3 position_;
	XMFLOAT3 target_;
	
	//Sistance will allow us to zoom further away and closer to the target position
	//The restraints will allow us to rotate alon an arc 180 degrees 
	//Keeps cam reaching a rotation beinf upside down
	float distance_, minDistance_, maxDistance_; 
	float xRotation_, yRotation_, yMin_, yMax_;
};
