#include "TokenStream.h"

bool isValidIdentifier(char c) {
	//Sees if the characer is a letter number or symbol. 
	//This is a default check for reading text from a file

	//ascii from ! to ~
	if ((int)c > 32 && (int)c < 127)
		return true;

	return false;
}

//Overloaded function
bool isValidIdentifier(char c, char* delimiters, int totalDelimiters) {
	//Checks the character with an array of desired delimiters.
	//This demo will open the sphere.obj model 
	//The only delimiters are new lines, whitespaces and /
	if (delimiters == 0 || totalDelimiters == 0)
		return isValidIdentifier(c);

	for (int i = 0; i < totalDelimiters; i++) {
		if (c == delimiters[i])
			return false;
	}

	return true;
}

TokenStream::TokenStream() {
	//Sets reading indices to 0
	ResetStream();
}

void TokenStream::ResetStream() {
	startIndex_ = endIndex_ = 0;
}

void TokenStream::SetTokenStream(char *data) {
	ResetStream();
	data_ = data;
}

bool TokenStream::GetNextToken(std::string* buffer, char* delimiters, int totalDelimiters) {

	//The start reading position becomes the end reading position, so, end of the file
	startIndex_ = endIndex_;

	//boolean to check if character is in srtring
	bool inString = false;
	//length of the text
	int length = (int)data_.length();

	//if the start reading position is bigger than the end of file
	if (startIndex_ >= length - 1)
		//False. No new token. Reached end of the data buffer.
		return false;

	//While start/current reading position is less than length of text so, there is still text to read
	//And character isn't a delimiter
	//Loops through text until it finds a delimiter.
	while (startIndex_ < length && isValidIdentifier(data_[startIndex_], delimiters, totalDelimiters) == false) {
		//Increent current reading position
		startIndex_++;
	}

	//Place to stop reading becomes the character just after the delmiter.
	endIndex_ = startIndex_ + 1;

	//If the character at the start index is a "... 
	if (data_[startIndex_] == '"')
		//then it is not in a string.
		inString = !inString;

	//If current read position (position just before delimiter) is not the end of the file....
	if (startIndex_ < length) {
		//While position just after delimiter is less than end of file 
		//AND characters are not delimiters or are defined as in the string...
		while (endIndex_ < length && (isValidIdentifier(data_[endIndex_], delimiters, totalDelimiters) || inString == true)) {
			//If end position character is "...
			if (data_[endIndex_] == '"')
				//Then defined as not in string
				inString = !inString;

			//Increment end reading position. 
			endIndex_++;
		}

		//if return string containing the token has a value
		if (buffer != NULL) {
			//Get size of string. Based on where token starts and ends.
			int size = (endIndex_ - startIndex_);
			//Reset index to start of token
			int index = startIndex_;

			buffer->reserve(size + 1);
			buffer->clear();

			for (int i = 0; i < size; i++) {
				//Push token from file into a vector
				//Token returned as first param, which is the address of the object that will return the token
				buffer->push_back(data_[index++]);
			}
		}

		return true;
	}

	return false;
}

//This will move from the current read indicees to the next line of data. 
//Return this line via pointer
//Do this because data is a continuous array of characters and we want our read indices to stay ready to read next token
//or to read the remainder of a line from its current position
bool TokenStream::MoveToNextLine(std::string* buffer) {
	//Get length of data file
	int length = (int)data_.length();

	//If start read position isn't end of file and end read position isn't end of file....
	if (startIndex_ < length && endIndex_ < length) {
		//End of read file equals start of read file
		endIndex_ = startIndex_;

		//End read position is less than the end of file.
		//AND check if character equals a blank space.
		while (endIndex_ < length && (isValidIdentifier(data_[endIndex_]) || data_[endIndex_] == ' ')) {
			//Increment end read position. Finding a blank space equals the end of a line.
			endIndex_++;
		}

		//The difference between the start and end read position is 0... 
		if ((endIndex_ - startIndex_) == 0)
			//Return no new line
			return false;

		//If the difference between he start and end read position is bigger than the length/end of the file
		if (endIndex_ - startIndex_ >= length)
			//Return no new line
			return false;
		
		//if return string contains a value. 
		if (buffer != NULL) {
			//Get size of string
			int size = (endIndex_ - startIndex_);
			//Reset index to start of file.
			int index = startIndex_;

			buffer->reserve(size + 1);
			buffer->clear();

			for (int i = 0; i < size; i++) {
				//Return resulting new line as pointer.
				buffer->push_back(data_[index++]);
			}
		}

	}

	else {
		return false;
	}

	endIndex_++;
	startIndex_ = endIndex_ + 1;

	return true;
}


