#pragma once

//TokenStream class will be used to return blocks of text from file

#include <string>

using namespace std;

//This will store entire file's data and the current read indices (start and end)
//These indices will mark the current positions it is reading from

class TokenStream
{
public:
	
	//The constructor and ResetStream set read indices to 0
	TokenStream();

	void ResetStream();

	//Will set data member variable that will store the file's
	void SetTokenStream(char* data);

	bool GetNextToken(std::string* buffer, char* delimiters, int totalDelimeters);

	bool MoveToNextLine(std::string *buffer);

private:

	int startIndex_, endIndex_;
	string data_;
};