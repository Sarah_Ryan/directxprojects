/*
Beginning DirectX 11 Game Programming
By Allen Sherrod and Wendy Jones

DirectX 11 Base Class - Used as base class for all DirectX 11 demos in this book.
*/


#include"Dx11DemoBase.h"
#include<d3dcompiler.h>


Dx11DemoBase::Dx11DemoBase() : driverType_(D3D_DRIVER_TYPE_NULL), featureLevel_(D3D_FEATURE_LEVEL_11_0),
d3dDevice_(0), d3dContext_(0), swapChain_(0), backBufferTarget_(0)
{

}


Dx11DemoBase::~Dx11DemoBase()
{
	Shutdown();
}


bool Dx11DemoBase::Initialize(HINSTANCE hInstance, HWND hwnd)
{
	hInstance_ = hInstance;
	hwnd_ = hwnd;

	RECT dimensions;

	//Calculates client area of app
	GetClientRect(hwnd, &dimensions);

	unsigned int width = dimensions.right - dimensions.left;
	unsigned int height = dimensions.bottom - dimensions.top;

	//Declares driver types and feature levels for devices. Tries to create hardware device. If fail - try other driver types and feature levels until find one supported

	//Array of driver types so we can loop through them to get the most desired device before continuing to others if failed
	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE, D3D_DRIVER_TYPE_SOFTWARE
	};


	unsigned int totalDriverTypes = ARRAYSIZE(driverTypes); //Returns size of array

	//Same as above array
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};

	unsigned int totalFeatureLevels = ARRAYSIZE(featureLevels);

	//Create swap chain desc and use info to find supported device type and feature level

	DXGI_SWAP_CHAIN_DESC swapChainDesc; //Swap chain description defines how we want it created
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc)); 
	swapChainDesc.BufferCount = 1; //Buffer count (Primary/secondary buffer for flipping)
	swapChainDesc.BufferDesc.Width = width; //buffer width
	swapChainDesc.BufferDesc.Height = height; //buffer height
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	//How often display is refreshed in Hertz
	//60/1 - 60Hz refresh rate
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60; 
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; //Swap chain can be used as output/can be rendered to
	swapChainDesc.OutputWindow = hwnd; //window handle - window we previously created with createWindowA
	swapChainDesc.Windowed = true; //If D3D should be windowed or full screen

	//Sample count and quality for sample description
	swapChainDesc.SampleDesc.Count = 1; 
	swapChainDesc.SampleDesc.Quality = 0;

	unsigned int creationFlags = 0;

#ifdef _DEBUG
	creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	HRESULT result;
	unsigned int driver = 0;

	for (driver = 0; driver < totalDriverTypes; ++driver)
	{

		//Function that creates swap chain, device and rendering context
		//Takes feature levels so if at least one exists and desired device exists - will work
		//Params: Pointer to video adapter - passing null = uses default device, driver type we wish to create
			//Handle to dll that implements software rendering device - if driver software - cannot be null, creation flags - 0 = release builds and 
			//create device debug for debugging, feature levels we wish to target ordered from most to least desired, number of feature levels in array, 
			//version - always same, swap chain desc object, address for device object, address of selected feature level - chosen one stored here, 
			//address for rendering context
		result = D3D11CreateDeviceAndSwapChain(0, driverTypes[driver], 0, creationFlags,
			featureLevels, totalFeatureLevels,
			D3D11_SDK_VERSION, &swapChainDesc, &swapChain_,
			&d3dDevice_, &featureLevel_, &d3dContext_);

		if (SUCCEEDED(result))
		{
			driverType_ = driverTypes[driver];
			break;
		}
	}

	if (FAILED(result))
	{
		DXTRACE_MSG("Failed to create the Direct3D device!");
		return false;
	}

	ID3D11Texture2D* backBufferTexture;

	//Get buffer obtains a pointer for rendering buffers
	//Params: buffer index, type of interface to manipulate, address of the buffer we are obtaining - MUST be LPVOID
	result = swapChain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferTexture);

	if (FAILED(result))
	{
		//Debugging macro 
		DXTRACE_MSG("Failed to get the swap chain back buffer!");
		return false;
	}


	//Creates render target view
	//Params: 2D texture we are creating view for,render target description, address of render target view we ae creating
	//Setting render desc to 0/NULL gives us a view of entire surface at mip level 0
	result = d3dDevice_->CreateRenderTargetView(backBufferTexture, 0, &backBufferTarget_);

	if (backBufferTexture)
		//After render target created, decrement back buffer pointer
		//Have to use release as it is a COM object
		backBufferTexture->Release();

	if (FAILED(result))
	{
		DXTRACE_MSG("Failed to create the render target view!");
		return false;
	}

	//Must set render target before drawing calls
	//Params: number of views we are binding in function call, list of render target views, depth/stencil views
	d3dContext_->OMSetRenderTargets(1, &backBufferTarget_, 0);

	//Viewport defines area of screen we are rendering to
	//Created by filling out object desc and then call context's RSSetViewports.
	D3D11_VIEWPORT viewport;
	viewport.Width = static_cast<float>(width);
	viewport.Height = static_cast<float>(height);
	viewport.MinDepth = 0.0f; //min depth of viewport
	viewport.MaxDepth = 1.0f; //max depth of viewport
	viewport.TopLeftX = 0.0f; //Mark left and...
	viewport.TopLeftY = 0.0f; //Top screen descriptions

	//Params: Number of viewports we are setting, list of viewport objects
	d3dContext_->RSSetViewports(1, &viewport);

	ZeroMemory(keyboardKeys_, sizeof(keyboardKeys_));
	ZeroMemory(prevKeyboardKeys_, sizeof(prevKeyboardKeys_));

	//Creates direct input object
	//This object provides interface to access DInput devices, can create instance of device, enumerate devices on system or check status of specific device
	//Params: Instance to app that is creating DInput object, version number of DInput app requires (default is shown), identifier of required interface (Default is shown),
	//pointer to variable that will hold created object, param usually set to null
	result = DirectInput8Create(hInstance_, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInput_, 0);
	if (FAILED(result)) {
		return false;
	}

	//Creates device
	//Gain specific access to an input device, whether that is keyboard, mouse, joystick etc
	//Params: SysKeyboard/SysMouse depending, address to store DInput device being created, dealing with COM interfaces. Mainly null
	//First param only covers if you want to use the system keyboard/mouse
	result = directInput_->CreateDevice(GUID_SysKeyboard, &keyboardDevice_, 0);
	if (FAILED(result)) {
		return false;
	}

	//Set data format that DInput will use to read input from device
	//Param: Is a (predefined) DIDATAFORMAT struct. Describes various elements of device. 
	//It is necessary to create own struct if input device you want is not a standard device but there are predefined ones for many devices.
	result = keyboardDevice_->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(result)) {
		return false;
	}

	//Needed to tell system how input device created will work with system. Set this for each device created
	//Can be exclusive (only your app has ability to use device, good for full screen) or...
	//Non-exclusive (Other apps can also use device, useful for windowed apps)
	//Params: handle to window requesting access, set of flags. Here it is in foreground so only requires input when app is current active window and non-exclusive.
	//Other flags are background, exclusive and NOWINKEY (disables windows key for app)
	result = keyboardDevice_->SetCooperativeLevel(hwnd_, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(result)) {
		return false;
	}

	//Acquire device. Provided with acess to initialise, get its capabilities or read its input
	//Tells system that device is ready to use and to read from
	result = keyboardDevice_->Acquire();
	if (FAILED(result)) {
		return false;
	}

	//Load content called after as standard D3D initialisation should happen first
	return LoadContent();
}

//Separated out because loading multiple shaders for different effects can cause redundant code
//We avoid this bu abstracting the behaviour in a member function of base clss DX11DemoBase
bool Dx11DemoBase::CompileD3DShader(char* filepath, char* entry, char* shaderModel, ID3DBlob** buffer) {

	DWORD shaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#if defined (DEBUG) || defined (_DEBUG)
	shaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* errorBuffer = 0;
	HRESULT result;

	result = D3DX11CompileFromFile(filepath, 0, 0, entry, shaderModel, shaderFlags, 0, 0, buffer, &errorBuffer, 0);

	if (FAILED(result)) {
		if (errorBuffer != 0)
		{
			OutputDebugStringA((char*)errorBuffer->GetBufferPointer());
			errorBuffer->Release();
		}
		return false;
	}

	if (errorBuffer != 0)
		errorBuffer->Release();

	return true;

}





bool Dx11DemoBase::LoadContent()
{
	// Override with demo specifics, if any...
	return true;
}


void Dx11DemoBase::UnloadContent()
{
	// Override with demo specifics, if any...
}


void Dx11DemoBase::Shutdown()
{
	UnloadContent();

	//Need to release resources so they are returned to system for reuse
	//Check objects not NULL and then release
	//Release in the reverse order to which they were created
	if (backBufferTarget_) backBufferTarget_->Release();
	if (swapChain_) swapChain_->Release();
	if (d3dContext_) d3dContext_->Release();
	if (d3dDevice_) d3dDevice_->Release();

	if (keyboardDevice_) {
		//If you don't unacquire devices, they may still be locked and unable to be used after application window closes
		keyboardDevice_->Unacquire();
		keyboardDevice_->Release();
	}

	if (directInput_) directInput_->Release();

	backBufferTarget_ = 0;
	swapChain_ = 0;
	d3dContext_ = 0;
	d3dDevice_ = 0;
	keyboardDevice_ = 0;
	directInput_ = 0;
}