#include<Windows.h>
#include<memory>
#include "TextureDemo.h"

//THIS PROJECT WILL OPEN A BLANK WHITE DX3D WINDOW WHEN RUN. CAN BE CLOSED BY PRESSING X.

//Windows proc function. Callback function - called whenever messages are being obtained and processed by app
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);


//WinMain performs the conversion between Unicode and ANSI for you, could lead to missing characters in Unicode string.
//Using wWinMain as it handles Unicode parameters, allows us to properly handle Unicode if they are passed

//hInstance = Handle of apps current instance
//prevInstance = Handle of previous app instance. Will ALWAYS be null. If you want to check if previous instance running, use a mutex called CreateMutex, will return "ERROR_ALREADY_EXISTS"
//cmdLine = app command line without name. Can pass commands to app e.g. from command line using command string etc.
//cmdShow = ID that specifies how window should be shown
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow) {

	//Used to avoid compiler warnings about parammeters that are unsused by function body
	UNREFERENCED_PARAMETER(prevInstance);
	UNREFERENCED_PARAMETER(cmdLine);

	//description of window class
	WNDCLASSEX wndClass = { 0 };
	wndClass.cbSize = sizeof(WNDCLASSEX); //Size of structure in bytes
	wndClass.style = CS_HREDRAW | CS_VREDRAW; //Style flags used to define window look
	wndClass.lpfnWndProc = WndProc; //Callback function called whenever event notif comes from OS. Will be function called WndProc, this is function pointer
	wndClass.hInstance = hInstance; //App instance that contains the windows procedure for this window class
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW); //Resource ID for graphic that will act as cursor
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Handle to background brush that will be used for painting windows background
	wndClass.lpszMenuName = NULL; //Resource name for the menu
	wndClass.lpszClassName = "DX11BookWindowClass"; //Name of window class

													//Apps must register its windows with the system
	if (!RegisterClassEx(&wndClass)) {
		return -1;
	}

	//Left and top represent starting location of window, right and bottom represent width and height 
	RECT rc = { 0,0,640,480 }; //bottom left, top left, top right, bottom right
							   //Calculate size required of window based on desired dimensions and style. If you want specific size, need to think about both client and non client areas (title bar, border etc)
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE); //Takes rect, style flag and boolean for if menu

													   //Create windowA accepts ANSI string params, create window accepts unicode
													   //Params: Windows class name, window title bar text, style flag, x pos, y pos, width, height, handle to parent window, resource handle to menu, app instance ID, data to be passed to the window and made available via lpParam of the windows callback function 
	HWND hwnd = CreateWindowA("DX11BookWindowClass", "Blank Direct3D 11 Window", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);

	if (!hwnd) {
		return -1;
	}

	//takes window handle returned by creatwindowA and the command show flag
	ShowWindow(hwnd, cmdShow);

	//Smart pointer, automatically deletes the memory it's pointing to when scope ends or when copied to another
	//Don't need to manually delete allocated data
	//It's exception safe - een if exception triggred and app halts, it will release data during stack unwinding. No leaks even if app crashes
	std::auto_ptr<Dx11DemoBase> demo(new TextureDemo());
	

	//Demo initialise
	bool result = demo->Initialize(hInstance, hwnd);
	
	//Error report if issue occurs
	if (result == false) {
		return -1;
	}
	//MSG holds window messages, some from OS, up to app to respond. If it doesn't after time, OS will say app is not responding. It MAY just be busy with tasks
	MSG msg = { 0 };

	while (msg.message != WM_QUIT) {
		//PeekMessage retrieves a message for the associated window matt was he re
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) { //Params: structure to hold message, window handle, min and max message filter flags and remove flag. Message is not in queue, it is being handled.
													 //Translates virtual key messages to character messages
			TranslateMessage(&msg);
			//Dispatches message to windows procedure function which will perform actions based on the message
			DispatchMessage(&msg);
		}
		else {
			//Update
			demo->Update(0.0f);
			//Draw
			demo->Render();
			
		}
	}

	//Demo shutdown
	demo->Shutdown();

	//Returns apps exit code. This is made into an int below.
	//static casts best used when casting numerical data
	return static_cast<int>(msg.wParam);


}

//Params: handle of window dispatching message, message as an unsigned int and 2 more params used to supply data to the function for messages that require more data
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {

	PAINTSTRUCT paintStruct;
	HDC hDC;

	switch (message) {

		//respond to paint message
	case WM_PAINT:
		//call win32 functions to draw window background
		hDC = BeginPaint(hwnd, &paintStruct);
		EndPaint(hwnd, &paintStruct);
		break;

		//respond to quit message
	case WM_DESTROY:
		//causes app to get WM_QUIT message, calls app loop to end and app will quit. Only used when you REALLY want to quit
		PostQuitMessage(0);
		break;

	default:
		//same params as windows proc. Called for messages we are not writing custom response to as it needs to respond to them
		return DefWindowProc(hwnd, message, wParam, lParam);
	}

	return 0;
}