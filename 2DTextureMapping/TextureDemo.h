#pragma once

#include "DX11DemoBase.h"

class TextureDemo : public Dx11DemoBase {

public:
	TextureDemo();
	virtual ~TextureDemo();

	bool LoadContent();
	void UnLoadContent();

	void Update(float dt);
	void Render();


private:

	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;
	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;

	//Object shaders use to access resources
	//When load tex into memory, must use it to access data via shader, will be binding to IA
	//CAN also provide general purpose data for parallel computing and other things
	//Allows us to view buffer in shader
	ID3D11ShaderResourceView* colorMap_;

	//Can access sampling state info of a tex
	//Allows us to set properties like tex filtering and addressing
	ID3D11SamplerState* colorMapSampler_;

};
