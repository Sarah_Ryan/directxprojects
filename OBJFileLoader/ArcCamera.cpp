#include <d3d11.h>
#include "ArcCamera.h"

ArcCamera::ArcCamera() : target_(XMFLOAT3(0.0f, 0.0f, 0.0f)),
position_(XMFLOAT3(0.0f, 0.0f, 0.0f)) {
	SetDistance(2.0f, 1.0f, 10.0f);
	SetRotation(0.0f, 0.0f, -XM_PIDIV2, XM_PIDIV2);
}

void ArcCamera::SetDistance(float distance, float minDistance, float maxDistance) {
	distance_ = distance;
	minDistance_ = minDistance;
	maxDistance_ = maxDistance;

	if (distance_ < minDistance_)distance_ = minDistance_;
	if (distance_ > maxDistance_)distance_ = maxDistance_;
}

void ArcCamera::SetRotation(float x, float y, float minY, float maxY) {
	xRotation_ = x;
	yRotation_ = y;
	yMin_ = minY;
	yMax_ = maxY;

	if (yRotation_ < yMin_) yRotation_ = yMin_;
	if (yRotation_ > yMax_)yRotation_ = yMax_;
}

void ArcCamera::SetTarget(XMFLOAT3& target) {
	target_ = target;
}

void ArcCamera::ApplyZoom(float zoomDelta) {
	distance_ += zoomDelta; //Increases or decreases the distance amount

	//Clamp result to the desired min and max distances
	if (distance_ < minDistance_) distance_ = minDistance_;
	if (distance_ > maxDistance_) distance_ = maxDistance_;
}

void ArcCamera::ApplyRotation(float yawDelta, float pitchDelta) { //Does same as ApplyZoom but to the rotation

	//Delta values are added
	//This is the change in values, not the absolute distance or rotation
	xRotation_ += yawDelta;
	yRotation_ += pitchDelta;

	//Rotation along x axis will control the camera moving up and down
	//So it's the axis that5 has limits applied to it. Preventing camera from going upside down

	if (xRotation_ < yMin_) yRotation_ = yMin_;
	if (xRotation_ > yMax_)yRotation_ = yMax_;
}

XMMATRIX ArcCamera::GetViewMatrix() {
	//Have already created position in local space
	//Call it's variable zoom, this is our camera's final position with no rotation
	//To transform this into world position we need to apply rotaton
	XMVECTOR zoom = XMVectorSet(0.0f, 0.0f, distance_, 1.0f);

	//Returns our rotation matrix
	//Takes yaw, pitch, roll values
	XMMATRIX rotation = XMMatrixRotationRollPitchYaw(xRotation_, -yRotation_, 0.0f);

	//Only need to rotate camera around target position
	//We translate our target to te target's position.
	//We rotate the local position by the camera's rotation matrix
	zoom = XMVector3Transform(zoom, rotation);

	XMVECTOR pos = XMLoadFloat3(&position_);
	XMVECTOR lookAt = XMLoadFloat3(&target_);

	//Then we translate (i.e. offset) the rotated camera's position around the target position. This is smple vector addition
	pos = lookAt + zoom;
	XMStoreFloat3(&position_, pos);

	//Calculating up vector.
	//Create local up vector (0,1,0)
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);
	//Rotate it by camera's rotation matrix to get true up vector
	up = XMVector3Transform(up, rotation);

	//Use calcuated position, target position and calculated up vector here
	//Creates the arc-ball controlled view matrix
	XMMATRIX viewMat = XMMatrixLookAtLH(pos, lookAt, up);

	return viewMat;
}