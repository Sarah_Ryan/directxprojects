#pragma once

#ifndef _OBJ_LOADER_H_
#define _OBJ_LOADER_H_

class ObjModel
{

	//This class uses TokenStream class to parse the data
	//Creates a triangle list of infrmation from it
public:
	ObjModel();
	~ObjModel();

	void Release();
	bool LoadOBJ(char *filename);

	float *GetVertices() {
		return vertices_;
	}

	float *GetNormals() {
		return normals_;
	}

	float *GetTexCoords() {
		return texCoords_;

	}



	int GetTotalVerts() { return totalVerts_; }

private:
	//OBJ file has vertex positions, texture co-ordinates and normal vectors
	//So this class has pointers for each
	float *vertices_;
	float *normals_;
	float *texCoords_;
	int totalVerts_;
};

#endif