#pragma once

#ifndef _GAME_SPRITE_DEMO_H_
#define _GAME_SPRITE_DEMO_H_

#include "DX11DemoBase.h"
#include "GameSprite.h"

class GameSpriteDemo : public Dx11DemoBase {
public:
	GameSpriteDemo();
	virtual ~GameSpriteDemo();

	bool LoadContent();
	void UnLoadContent();

	void Update(float dt);
	void Render();

private:
	//Explanatons in texture mapping demo or older projects
	ID3D11VertexShader* solidColorVS_;
	ID3D11PixelShader* solidColorPS_;

	ID3D11InputLayout* inputLayout_;
	ID3D11Buffer* vertexBuffer_;

	ID3D11ShaderResourceView* colorMap_;
	ID3D11SamplerState* colorMapSampler_;

	//New blend state object
	//Use it in order to render sprites using alpha transparency. Must use 32-bit tex images with an alpha channel
	ID3D11BlendState* alphaBlendState_;

	//Array of sprite resources that will be drawn
	GameSprite sprites_[3]; 

	//Constant buffer to pass data to vertex shader
	//Use to send model view projection matrix to vertex shader so it can transform incoming geometry
	//Is no camera and projection matrix doesn't change unless window resizes, can calculate it once and use it in render function to calculate
	//full model projection matrix
	ID3D11Buffer* mvpCB_; 



	XMMATRIX vpMatrix_; //View projection matrix
}; 

#endif