#ifndef _BLANK_DEMO_H_
#define _BLANK_DEMO_H_

#include"DX11DemoBase.h"


class BlankDemo : public Dx11DemoBase
{
public:

	//Mostly empty functions as program just clears screen using D3D
	BlankDemo();
	virtual ~BlankDemo();

	bool LoadContent();
	void UnloadContent();

	//dt param represents the time delta passed since last frame
	//Used for time based updates
	void Update(float dt);
	void Render();
};

#endif
