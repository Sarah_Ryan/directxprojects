#include<d3d11.h>
#include <D3DX11.h>
#include "GameSprite.h"

//Sets up way to store an individual sprites position, rotation and scale
//As well as a way to build Sprite's world (model) view


GameSprite::GameSprite() : rotation_(0) {
	//Scale is set to 1 fr both axis...
	//because anything less will shrink, anything more will make it bigger
	//1 keeps sprites size unchanged
	scale_.x = scale_.y = 1.0f;
}

GameSprite::~GameSprite() {

}

XMMATRIX GameSprite::GetWorldMatrix() {

	//Params: X, Y and Z values
	XMMATRIX translation = XMMatrixTranslation(position_.x, position_.y, 0.0f);

	XMMATRIX rotationZ = XMMatrixRotationZ(rotation_);
	XMMATRIX scale = XMMatrixScaling(scale_.x, scale_.y, 1.0f);

	return translation * rotationZ * scale;
};

void GameSprite::SetPosition(XMFLOAT2& position) {
	position_ = position;
}

void GameSprite::SetRotation(float rotation) {
	rotation_ = rotation;
}

void GameSprite::SetScale(XMFLOAT2& scale) {
	scale_ = scale;
}

